import GfG.arePermutation
import java.util.*

internal object GfG {
    /* function to check whether two strings are
Permutation of each other */
    fun arePermutation(str1: String, str2: String): Boolean {
        // Get lenghts of both strings

        val n1 = str1.length
        val n2 = str2.length

        // If length of both strings is not same,
        // then they cannot be Permutation


        if (n1 != n2) return false
        val ch1 = str1.toCharArray()
        val ch2 = str2.toCharArray()

        // Sort both strings


        Arrays.sort(ch1)
        Arrays.sort(ch2)

        // Compare sorted strings


        for (i in 0 until n1) if (ch1[i] != ch2[i]) return false
        return true
    }
}

fun main(args: Array<String>) {
    val str1 = "test"
    val str2 = "ttew"
    if (arePermutation(str1, str2)) println("Yes") else println("No")
}